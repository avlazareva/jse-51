package ru.t1.lazareva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends IDtoRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    boolean existsById(@NotNull String userId, @NotNull String id) throws Exception;

    boolean existsByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    void remove(@NotNull String userId, @NotNull M model) throws Exception;

    void removeById(@NotNull String userId, @NotNull String id) throws Exception;

    void removeByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    M update(@NotNull String userId, @NotNull M model) throws Exception;

}
