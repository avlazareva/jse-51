package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.model.ITaskRepository;
import ru.t1.lazareva.tm.comparator.CreatedComparator;
import ru.t1.lazareva.tm.comparator.NameComparator;
import ru.t1.lazareva.tm.comparator.StatusComparator;
import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Task task = getClazz().newInstance();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Task task = getClazz().newInstance();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    protected Class<Task> getClazz() {
        return Task.class;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @Override
    public void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m WHERE m.user.id = :userId and m.project.id = :projectId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
