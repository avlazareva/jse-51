package ru.t1.lazareva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.entity.TaskNotFoundException;
import ru.t1.lazareva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.lazareva.tm.exception.field.TaskIdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.lazareva.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private IProjectDtoRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @NotNull
    private ITaskDtoRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDto task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
            @Nullable ProjectDto project = projectRepository.findOneById(userId, projectId);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks == null) throw new TaskNotFoundException();
            entityManager.getTransaction().begin();
            for (final TaskDto task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDto task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}