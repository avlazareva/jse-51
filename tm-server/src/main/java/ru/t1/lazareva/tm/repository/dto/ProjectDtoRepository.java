package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.lazareva.tm.dto.model.ProjectDto;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<ProjectDto> getEntityClass() {
        return ProjectDto.class;
    }

    @NotNull
    @Override
    public ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final ProjectDto project = new ProjectDto(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDto create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final ProjectDto project = new ProjectDto(name);
        return add(userId, project);
    }

}


