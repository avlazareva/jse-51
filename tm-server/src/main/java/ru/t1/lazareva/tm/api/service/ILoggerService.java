package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String message);

    void command(@Nullable String message);

    @SuppressWarnings("unused")
    void debug(@Nullable String message);

    void error(Exception e);

    void initJmsLogger();
}
