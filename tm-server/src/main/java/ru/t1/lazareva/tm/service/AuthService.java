package ru.t1.lazareva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.service.IAuthService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.ISessionDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.AccessDeniedException;
import ru.t1.lazareva.tm.util.CryptUtil;
import ru.t1.lazareva.tm.util.HashUtil;

import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserDtoService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionDtoService sessionService;

    public AuthService(
            @NotNull IPropertyService propertyService,
            @NotNull IUserDtoService userService,
            @NotNull ISessionDtoService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    private SessionDto createSession(@NotNull final UserDto user) throws Exception {
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    @Override
    public UserDto registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) throws Exception {
        return userService.create(login, password, email);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@Nullable final UserDto user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@Nullable final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }


    @NotNull
    @Override
    @SneakyThrows
    public SessionDto validateToken(@Nullable final String token) throws Exception {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDto session = objectMapper.readValue(json, SessionDto.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final long timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        if (!sessionService.existsById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable final SessionDto session) throws Exception {
        if (session == null) return;
        sessionService.removeById(session.getUserId(), session.getId());
    }


    @NotNull
    @Override
    public UserDto check(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = userService.findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

}