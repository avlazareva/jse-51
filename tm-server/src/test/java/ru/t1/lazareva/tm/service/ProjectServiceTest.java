package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.field.DescriptionEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.NameEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDtoService SERVICE = new ProjectDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDtoService TASK_SERVICE = new TaskDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDtoService USER_SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
        SERVICE.add(USER_1.getId(), USER_PROJECT1);
        SERVICE.add(USER_2.getId(), USER_PROJECT2);
    }

    @After
    public void after() throws Exception {
        SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Test
    public void add() throws Exception {
        SERVICE.clear();
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_PROJECT1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.add(USER_PROJECT1.getUserId(), null);
        });
        SERVICE.clear();
        Assert.assertNotNull(SERVICE.add(USER_PROJECT1.getUserId(), USER_PROJECT1));
        @Nullable final ProjectDto project = SERVICE.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.add(USER_1.getId(), NULL_PROJECT);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_PROJECT1);
        });
        SERVICE.clear();
        Assert.assertNotNull(SERVICE.add(USER_1.getId(), USER_PROJECT1));
        @Nullable final ProjectDto project = SERVICE.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<ProjectDto> projectsFindAllNoEmpty = SERVICE.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(""));
        Assert.assertFalse(SERVICE.existsById(null));
        Assert.assertFalse(SERVICE.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(SERVICE.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.existsById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.existsById(USER_1.getId(), "");
        });
        Assert.assertFalse(SERVICE.existsById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(SERVICE.existsById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.findOneById(NON_EXISTING_PROJECT_ID);
        });
        @Nullable final ProjectDto project = SERVICE.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(USER_1.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", USER_PROJECT1.getId());
        });
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDto project = SERVICE.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void clear() throws Exception {
        SERVICE.clear();
        Assert.assertEquals(0, SERVICE.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear("");
        });
        SERVICE.clear();
        SERVICE.add(USER_1.getId(), USER_PROJECT1);
        SERVICE.clear(USER_1.getId());
        Assert.assertEquals(0, SERVICE.getSize(USER_1.getId()));
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(null);
        });
        @NotNull final ProjectDto createdProject = SERVICE.create(USER_1.getId(), USER_PROJECT1.getName());
        SERVICE.remove(createdProject);
        Assert.assertNotEquals(0, SERVICE.findAll(USER_PROJECT1.getUserId()).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.remove("", null);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(USER_1.getId(), null);
        });
        SERVICE.clear();
        @NotNull final ProjectDto createdProject = SERVICE.create(USER_1.getId(), USER_PROJECT1.getName());
        SERVICE.remove(USER_1.getId(), createdProject);
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(USER_1.getId(), "");
        });
        SERVICE.removeById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
        @Nullable final ProjectDto createdProject = SERVICE.add(USER_PROJECT1);
        SERVICE.removeById(USER_1.getId(), createdProject.getId());
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        SERVICE.clear();
        SERVICE.add(USER_PROJECT1);
        Assert.assertEquals(1, SERVICE.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize("");
        });
        SERVICE.clear();
        Assert.assertTrue(SERVICE.findAll().isEmpty());
        Assert.assertEquals(0, SERVICE.getSize(USER_1.getId()));
        SERVICE.add(USER_PROJECT1);
        Assert.assertEquals(1, SERVICE.getSize(USER_1.getId()));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_PROJECT1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_PROJECT1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), "");
        });
        @NotNull final ProjectDto project = SERVICE.create(USER_1.getId(), USER_PROJECT1.getName());
        Assert.assertEquals(project.getId(), SERVICE.findOneById(USER_1.getId(), project.getId()).getId());
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_1.getId(), project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), USER_PROJECT1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(USER_1.getId(), USER_PROJECT1.getName(), "");
        });
        @NotNull final ProjectDto project = SERVICE.create(USER_1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        Assert.assertEquals(project.getId(), SERVICE.findOneById(USER_1.getId(), project.getId()).getId());
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_1.getId(), project.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(USER_1.getId(), USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.updateById(USER_1.getId(), NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName();
        @NotNull final String description = USER_PROJECT1.getDescription();
        SERVICE.updateById(USER_1.getId(), USER_PROJECT1.getId(), name, description);
        Assert.assertEquals(name, USER_PROJECT1.getName());
        Assert.assertEquals(description, USER_PROJECT1.getDescription());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.NOT_STARTED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(USER_1.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(USER_1.getId(), "", status);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.changeProjectStatusById(USER_1.getId(), NON_EXISTING_PROJECT_ID, status);
        });
        SERVICE.changeProjectStatusById(USER_1.getId(), USER_PROJECT1.getId(), status);
        Assert.assertNotNull(USER_PROJECT1);
        Assert.assertEquals(status, USER_PROJECT1.getStatus());
    }

}