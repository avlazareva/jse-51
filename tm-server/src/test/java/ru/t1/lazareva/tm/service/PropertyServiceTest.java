package ru.t1.lazareva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.ILoggerService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private static final IPropertyService SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(SERVICE);

    @NotNull
    private final ILoggerService loggerService = new LoggerService(SERVICE, CONNECTION_SERVICE);

    @Test
    public void getApplicationConfig() {
        Assert.assertNotNull(SERVICE.getApplicationConfig());
    }

    @Test
    public void getApplicationVersion() {
        Assert.assertNotNull(SERVICE.getApplicationVersion());
    }

    @Test
    public void getAuthorEmail() {
        Assert.assertNotNull(SERVICE.getAuthorEmail());
    }

    @Test
    public void getAuthorName() {
        Assert.assertNotNull(SERVICE.getAuthorName());
    }

    @Test
    public void getGitBranch() {
        Assert.assertNotNull(SERVICE.getGitBranch());
    }

    @Test
    public void getGitCommitId() {
        Assert.assertNotNull(SERVICE.getGitCommitId());
    }

    @Test
    public void getGitCommitterName() {
        Assert.assertNotNull(SERVICE.getGitCommitterName());
    }

    @Test
    public void getGitCommitterEmail() {
        Assert.assertNotNull(SERVICE.getGitCommitterEmail());
    }

    @Test
    public void getGitCommitMessage() {
        Assert.assertNotNull(SERVICE.getGitCommitMessage());
    }

    @Test
    public void getGitCommitTime() {
        Assert.assertNotNull(SERVICE.getGitCommitTime());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(SERVICE.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(SERVICE.getServerHost());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(SERVICE.getSessionTimeout());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(SERVICE.getSessionKey());
    }

}