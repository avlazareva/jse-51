package ru.t1.lazareva.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.lazareva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.ITaskDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.repository.dto.ProjectDtoRepository;
import ru.t1.lazareva.tm.repository.dto.TaskDtoRepository;
import ru.t1.lazareva.tm.repository.dto.UserDtoRepository;
import ru.t1.lazareva.tm.service.ConnectionService;
import ru.t1.lazareva.tm.service.PropertyService;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;

import static ru.t1.lazareva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final EntityManager ENTITY_MANAGER = CONNECTION_SERVICE.getEntityManager();

    @NotNull
    private static final ITaskDtoRepository TASK_REPOSITORY = new TaskDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final IUserDtoRepository REPOSITORY = new UserDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final IUserDtoRepository EMPTY_REPOSITORY = new UserDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final IDtoRepository DTO_REPOSITORY = new ProjectDtoRepository(ENTITY_MANAGER);

    @NotNull
    private static final IProjectDtoService SERVICE = new ProjectDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDtoService TASK_SERVICE = new TaskDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDtoService USER_SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        USER_SERVICE.remove(USER_1);
        USER_SERVICE.remove(USER_2);
        ENTITY_MANAGER.close();
    }

    @Before
    @SneakyThrows
    public void before() {
        if (ENTITY_MANAGER.getTransaction().isActive())
            ENTITY_MANAGER.getTransaction().rollback();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final UserDto user : USER_LIST) {
            try {
                ENTITY_MANAGER.getTransaction().begin();
                REPOSITORY.remove(user);
                ENTITY_MANAGER.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                ENTITY_MANAGER.getTransaction().rollback();
            }
        }
        try {
            ENTITY_MANAGER.getTransaction().begin();
            REPOSITORY.clear();
            ENTITY_MANAGER.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            ENTITY_MANAGER.getTransaction().rollback();
        }
    }

    @Test
    public void add() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add((USER_1));
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final UserDto user = REPOSITORY.findOneById(USER_1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_1.getId(), user.getId());
    }

    @Test
    public void findAll() {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(REPOSITORY.findAll());
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final UserDto createdUser = USER_1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(createdUser);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertFalse(REPOSITORY.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(REPOSITORY.existsById(USER_1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final UserDto user1 = REPOSITORY.findOneById(USER_1.getId());
        Assert.assertNotNull(user1);
        Assert.assertEquals(USER_1.getId(), user1.getId());
        @Nullable final UserDto user2 = REPOSITORY.findOneById("");
        Assert.assertNull(user2);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final UserDto createdUser = USER_1;
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(createdUser);
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.clear();
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(0, REPOSITORY.getSize());
    }

    @Test
    public void remove() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final UserDto createdUser = REPOSITORY.create(USER_1.getLogin(), USER_1.getPasswordHash());
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertNotNull(createdUser);
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.remove(createdUser);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final UserDto user = REPOSITORY.findOneById(createdUser.getId());
        Assert.assertNull(user);
    }

    @Test
    public void getSize() throws Exception {
        Assert.assertTrue(REPOSITORY.findAll().isEmpty());
        Assert.assertEquals(0, REPOSITORY.getSize());
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        Assert.assertEquals(1, REPOSITORY.getSize());
    }

    @Test
    public void findByLogin() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final UserDto user = REPOSITORY.findByLogin(USER_1.getLogin());
        Assert.assertEquals(USER_1.getLogin(), user.getLogin());
    }

    @Test
    public void findByEmail() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final UserDto user = REPOSITORY.findByEmail(USER_1.getEmail());
        Assert.assertEquals(USER_1.getEmail(), user.getEmail());
    }

    @Test
    public void isLoginExists() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final UserDto user = REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final Boolean loginExists = REPOSITORY.isLoginExists(user.getLogin());
        Assert.assertTrue(loginExists);
    }

    @Test
    public void isEmailExists() throws Exception {
        ENTITY_MANAGER.getTransaction().begin();
        @Nullable final UserDto user = REPOSITORY.add(USER_1);
        ENTITY_MANAGER.getTransaction().commit();
        @Nullable final Boolean emailExists = REPOSITORY.isEmailExists(user.getEmail());
        Assert.assertTrue(emailExists);
    }

}
